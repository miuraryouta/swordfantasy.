#pragma once
#include "Engine/GameObject.h"

//プレイヤーを管理するクラス
class Player : public GameObject
{
	int hModel_;    //モデル番号
	int hSound_;    //サウンド番号

	//移動処理
	//引数：なし
	//戻り値：なし
	void Move();

	//特殊
	//引数：なし
	//戻り値：なし
	void Special();

public:



	//コンストラクタ
	Player(GameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	int GetModelHandle() { return hModel_; }

	//ステータス
	int HP_PL = 100; //体力

	int MP_PL = 50; //マジックポイント

	int ST_PL = 100;  //スタミナ

	int SP_PL = 50;   //スペシャル

	//フラグ
	int flag1 = 0;

	int flag2 = 0;

	int flag3 = 0;

	int flag4 = 0;

	int flag5 = 0;

	int flag6 = 0;   //スタミナのやつ

	int flag7 = 0;    //攻撃用フラグ

	//死んだときのフラグ
	//何回か死んだら敵が弱くなるようにしたい
	int flagDead_SL = 0;

	//タイム
	int time1;

	int time2;

	int time3;

	int time4;

	int time5;

	int time6;

	int time7;

	char TimerFlag_1 = 30;

	char TimerFlag_2 = 1;

	char TimerFlag_3 = 1;

	char TimerFlag_4 = 2;

	char TimerFlag_5 = 1;

	char TimerFlag_6 = 1;

	char TimerFlag_7 = 1;

	XMVECTOR PGetPosition = transform_.position_;

	

	//何かに当たった
//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void CountDown();



};