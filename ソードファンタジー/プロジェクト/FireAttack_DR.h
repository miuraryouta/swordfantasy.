#pragma once
#include "Engine/GameObject.h"

//炎攻撃を管理するクラス
class FireAttack_DR : public GameObject
{

    int hModel_;    //モデル番号

    int time;

    char TimerFlag_FAD = 5;

public:
    //コンストラクタ
    FireAttack_DR(GameObject* parent);

    //デストラクタ
    ~FireAttack_DR();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //何かに当たった
    //引数：pTarget 当たった相手
    void OnCollision(GameObject* pTarget) override;


    //炎の攻撃力
    int AT_FA = 20;

    //フラグ
    int flag = 0;

    int flag2 = 5;


};