#include "GameOverScene1.h"
#include "Engine/Image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"

//コンストラクタ
GameOverScene1::GameOverScene1(GameObject * parent)
	: GameObject(parent, "GameOverScene1"), hPict_(-1)
{
}

//初期化
void GameOverScene1::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("GameOver.png");
	assert(hPict_ >= 0);

	transform_.scale_.vecX = 3.0f;
	transform_.scale_.vecY = 3.0f;
}

//更新
void GameOverScene1::Update()
{
	//エンターキーが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}

	//スペースキーが押されていたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY1);
	}
}

//描画
void GameOverScene1::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameOverScene1::Release()
{
}