#pragma once
#include "Engine/GameObject.h"

//STバーを管理するクラス
class STBar : public GameObject
{
	int hPict_[101];    //画像番号

	int STflag = 100;

public:



	//コンストラクタ
	STBar(GameObject* parent);

	//デストラクタ
	~STBar();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


};