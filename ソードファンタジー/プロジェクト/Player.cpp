#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Engine/SceneManager.h"
#include "Sword.h"
#include "FireMagic.h"
#include "Engine/BoxCollider.h"
#include "Slime.h"
#include "Dog.h"
#include "Goblin.h"
#include "bird.h"
#include "Minotaur.h"
#include "Dragon.h"
#include "FireAttack_DR.h"
#include "Engine/Audio.h"
#include "Ground.h"



//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), hModel_(-1), hSound_(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);

	//サウンドをロードするとエラーが起こる
	//サウンドデータのロード
	//hSound_ = Audio::Load("FireMagic1.wav");
	//assert(hSound_ >= 0);

	Instantiate<Sword>(this);
	Instantiate<FireMagic>(this);


	//初期位置
	transform_.position_.vecX = 0;
	transform_.position_.vecY = 0;
	transform_.position_.vecZ = -25;

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 1.5, 0, 0), XMVectorSet(2, 2, 2, 0));
	AddCollider(collision);

	time1 = 30;

	time2 = 1;

	time3 = 1;

	time4 = 2;

	time5 = 1;

	time6 = 1;

	time7 = 1;

	Model::SetAnimFrame(hModel_, 1, 150, 2);

}

//更新
void Player::Update()
{
	Move();


	Special();


}

void Player::Move()
{
	Slime* pSlime_ = (Slime*)FindObject("Slime");
	Dog* pDog_ = (Dog*)FindObject("Dog");
	Goblin* pGoblin_ = (Goblin*)FindObject("Goblin");
	bird* pbird_ = (bird*)FindObject("bird");
	Minotaur* pMinotaur_ = (Minotaur*)FindObject("Minotaur");
	Dragon* pDragon_ = (Dragon*)FindObject("Dragon");
	FireAttack_DR* pFAR_ = (FireAttack_DR*)FindObject("FireAttack_DR");

	XMVECTOR move_w = { 0.0f, 0.0f, 0.2f, 0.0f };
	XMVECTOR move_d = { 0.2f, 0.0f, 0.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_w = XMVector3TransformCoord(move_w, mat);
	move_d = XMVector3TransformCoord(move_d, mat);

	//Wキーを押した
	if (Input::IsKey(DIK_W))
	{
		//前に進む
		transform_.position_ += move_w;

		

		//Zキーが押されたら
		if (Input::IsKeyDown(DIK_Z))
		{
			
			//回避する

			if (flag2 == 0)
			{
				pSlime_->AT_SL = 0;
				pDog_->AT_DG = 0;
				pGoblin_->AT_GB = 0;
				pbird_->AT_BI = 0;
				pMinotaur_->AT_MI = 0;
				pDragon_->AT_DR = 0;
				pFAR_->AT_FA = 0;

				flag2 = 1;
			}

			transform_.position_.vecZ = transform_.position_.vecZ + 3;



		}

	}

	//Aキーを押した
	if (Input::IsKey(DIK_A))
	{
		//左に進む
		transform_.position_ -= move_d;

		

		//Zキーが押されたら
		if (Input::IsKeyDown(DIK_Z))
		{
			//回避する

			if (flag2 == 0)
			{
				pSlime_->AT_SL = 0;
				pDog_->AT_DG = 0;
				pGoblin_->AT_GB = 0;
				pbird_->AT_BI = 0;
				pMinotaur_->AT_MI = 0;
				pDragon_->AT_DR = 0;
				pFAR_->AT_FA = 0;

				flag2 = 1;
			}

			transform_.position_.vecX = transform_.position_.vecX - 3;
		}
	}


	//Sキーを押した
	if (Input::IsKey(DIK_S))
	{
		//後ろに進む
		transform_.position_ -= move_w;

		//Zキーが押されたら
		if (Input::IsKeyDown(DIK_Z))
		{
			//回避する

			if (flag2 == 0)
			{
				pSlime_->AT_SL = 0;
				pDog_->AT_DG = 0;
				pGoblin_->AT_GB = 0;
				pbird_->AT_BI = 0;
				pMinotaur_->AT_MI = 0;
				pDragon_->AT_DR = 0;
				pFAR_->AT_FA = 0;

				flag2 = 1;
			}

			transform_.position_.vecZ = transform_.position_.vecZ - 3;
		}
	}

	//Dキーを押した
	if (Input::IsKey(DIK_D))
	{
		//右に進む
		transform_.position_ += move_d;

		

		//Zキーが押されたら
		if (Input::IsKeyDown(DIK_Z))
		{
			//回避する

			if (flag2 == 0)
			{
				pSlime_->AT_SL = 0;
				pDog_->AT_DG = 0;
				pGoblin_->AT_GB = 0;
				pbird_->AT_BI = 0;
				pMinotaur_->AT_MI = 0;
				pDragon_->AT_DR = 0;
				pFAR_->AT_FA = 0;

				flag2 = 1;
			}

			transform_.position_.vecX = transform_.position_.vecX + 3;
		}
	}


	//右矢印キーを押した
	if (Input::IsKey(DIK_RIGHT))
	{
		//右に回転
		transform_.rotate_.vecY += 1.0f;
	}

	//左矢印キーを押した
	if (Input::IsKey(DIK_LEFT))
	{
		//左に回転
		transform_.rotate_.vecY -= 1.0f;
	}





		//左シフトキーが押されていたら
		if (Input::IsKey(DIK_LSHIFT))
		{

				if (Input::IsKey(DIK_W))
				{


					//スタミナが0より大きい時だけダッシュする
					if (ST_PL > 0)
					{
						transform_.position_ += move_w * 2;
						//ST_PL -= 5;
					}

					if (flag5 == 0)
					{
						ST_PL -= 5;
						flag5 = 1;
					}



				}

				

				if (Input::IsKey(DIK_S))
				{


					//スタミナが0より大きい時だけダッシュする
					if (ST_PL > 0)
					{
						transform_.position_ -= move_w * 2;
						//ST_PL = -5;
					}

					if (flag5 == 0)
					{
						ST_PL -= 5;
						flag5 = 1;
					}

				}

				if (Input::IsKey(DIK_A))
				{


					//スタミナが0より大きい時だけダッシュする
					if (ST_PL > 0)
					{
						transform_.position_ -= move_d * 2;
						//ST_PL = -5;
					}

					if (flag5 == 0)
					{
						ST_PL -= 5;
						flag5 = 1;
					}

				}

				if (Input::IsKey(DIK_D))
				{


					//スタミナが0より大きい時だけダッシュする
					if (ST_PL > 0)
					{
						transform_.position_ += move_d * 2;
						//ST_PL -= 5;
					}

					if (flag5 == 0)
					{
						ST_PL -= 5;
						flag5 = 1;
					}
				}
			


		}
		else
		{
			
			if (flag3 == 0 && flag5 == 0)
			{


				if (ST_PL < 100)
				{
					ST_PL = ST_PL + 10;  //ダッシュしていないときはスタミナが回復する
					flag3 = 1;
				}
			}


			if (flag3 == 1)
			{
				//スタミナは1秒ごとに回復する
				timeBeginPeriod(1);
				static DWORD lastFpsResetTime = timeGetTime();
				DWORD nowTime = timeGetTime();
				if (nowTime - lastFpsResetTime > 1000)
				{
					while (time3 == TimerFlag_3)
					{
						switch (time3)
						{

						case 1:TimerFlag_3 = 1; break;
						case 0:TimerFlag_3 = 0;
						}
						time3--;
						TimerFlag_3--;
						break;
					}
					lastFpsResetTime = nowTime;
				}
				if (TimerFlag_3 == 0)
				{
					flag3 = 0;
					time3 = 1;
					TimerFlag_3 = 1;
				}
			}

		}
		
		//


		//スタミナは1秒ごとに5減る
		if (flag5 == 1)
		{
			


			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time5 == TimerFlag_5)
				{
					switch (time5)
					{
					case 1:TimerFlag_5 = 1; break;
					case 0:TimerFlag_5 = 0;
					}
					time5--;
					TimerFlag_5--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_5 == 0)
			{
				flag5 = 0;
				time5 = 1;
				TimerFlag_5 = 1;

			}
		}
	

		/*
	if (flag6 == 1)
	{
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time6 == TimerFlag_6)
			{
				switch (time6)
				{
				case 1:TimerFlag_6 = 1; break;
				case 0:TimerFlag_6 = 0;
				}
				time6--;
				TimerFlag_6--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_6 == 0)
		{
			flag6 = 0;
			time6 = 1;
			TimerFlag_6 = 1;
			
		}
	}
	*/
	
	

	//スタミナは100以上回復しない
	if (ST_PL > 100)
	{
		int a = ST_PL - 100;
		ST_PL = ST_PL - a;
	}

	//スタミナは0以下にならない
	if (ST_PL < 0)
	{
		int j = 0 - ST_PL;
		ST_PL = ST_PL + j;
	}

	//マジックポイントは50以上回復しない
	if (MP_PL > 50)
	{
		int m = MP_PL - 50;
		MP_PL = MP_PL - m;
	}

	//カメラの移動
	Camera::SetTarget(transform_.position_ + XMVectorSet(0, 5, 0, 0));

	XMVECTOR camVec = { 0.0f, 8.0f, -10.0f };
	camVec = XMVector3TransformCoord(camVec, mat);
	Camera::SetPosition(transform_.position_ + camVec);
}

void Player::Special()
{
	Sword* pSword_ = (Sword*)FindObject("Sword");
	FireMagic* pFireMagic_ = (FireMagic*)FindObject("FireMagic");

	if (SP_PL < 100)
	{
		if (flag4 == 0)
		{
			if (flag1 == 0)
			{
				SP_PL += 1;
				flag4 = 1;
			}
		}
		else if (flag4 == 1)
		{
			//1秒ごとのほうがいいかもしれない
			//SP_PLは2秒ごとに1回復する
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time4 == TimerFlag_4)
				{
					switch (time4)
					{
					case 2:TimerFlag_4 = 2; break;
					case 1:TimerFlag_4 = 1; break;
					case 0:TimerFlag_4 = 0;
					}
					time4--;
					TimerFlag_4--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_4 == 0)
			{

				flag4 = 0;
				time4 = 2;
				TimerFlag_4 = 2;
			}
		}
	}

	if (SP_PL > 100)
	{
		int a = SP_PL - 100;
		SP_PL = SP_PL - a;
	}

	if (SP_PL == 100)
	{
		if (Input::IsKey(DIK_M))
		{
			if (flag1 == 0)
			{
				//ハイパーモード
				pSword_->AT_S = pSword_->AT_S * 2;     //攻撃力が2倍になる
				pFireMagic_->AT_FM = pFireMagic_->AT_FM * 2;   //攻撃力が2倍になる
				SP_PL -= 100;
				flag1 = 1;
			}

		}


	}

	if (flag1 == 1)
	{
		//ハイパーモードは30秒間だけ発動できる
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time1 == TimerFlag_1)
			{
				switch (time1)
				{
				case 30:TimerFlag_1 = 30; break;
				case 29:TimerFlag_1 = 29; break;
				case 28:TimerFlag_1 = 28; break;
				case 27:TimerFlag_1 = 27; break;
				case 26:TimerFlag_1 = 26; break;
				case 25:TimerFlag_1 = 25; break;
				case 24:TimerFlag_1 = 24; break;
				case 23:TimerFlag_1 = 23; break;
				case 22:TimerFlag_1 = 22; break;
				case 21:TimerFlag_1 = 21; break;
				case 20:TimerFlag_1 = 20; break;
				case 19:TimerFlag_1 = 19; break;
				case 18:TimerFlag_1 = 18; break;
				case 17:TimerFlag_1 = 17; break;
				case 16:TimerFlag_1 = 16; break;
				case 15:TimerFlag_1 = 15; break;
				case 14:TimerFlag_1 = 14; break;
				case 13:TimerFlag_1 = 13; break;
				case 12:TimerFlag_1 = 12; break;
				case 11:TimerFlag_1 = 11; break;
				case 10:TimerFlag_1 = 10; break;
				case 9:TimerFlag_1 = 9; break;
				case 8:TimerFlag_1 = 8; break;
				case 7:TimerFlag_1 = 7; break;
				case 6:TimerFlag_1 = 6; break;
				case 5:TimerFlag_1 = 5; break;
				case 4:TimerFlag_1 = 4; break;
				case 3:TimerFlag_1 = 3; break;
				case 2:TimerFlag_1 = 2; break;
				case 1:TimerFlag_1 = 1; break;
				case 0:TimerFlag_1 = 0;
				}
				time1--;
				TimerFlag_1--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_1 == 0)
		{

			pSword_->AT_S = pSword_->AT_S / 2;  //攻撃力が元に戻る
			pFireMagic_->AT_FM = pFireMagic_->AT_FM / 2;    //攻撃力が元に戻る
			SP_PL = 0;    //SPが0になる
			flag1 = 0;
			time1 = 30;
			TimerFlag_1 = 30;
		}
	}


	//





	//Qキーが押されたら
	if (Input::IsKeyDown(DIK_Q))
	{
		if (MP_PL > 5)
		{
		//魔法の音が鳴る
		//Audio::Play(hSound_);

			



			//魔法が飛ぶ
			FireMagic* pFireMagic = Instantiate<FireMagic>(GetParent());
			pFireMagic->SetPosition(transform_.position_);
			MP_PL = MP_PL - 5;

			//ボーンを設定する
			XMVECTOR shotPos = Model::GetBonePosition(hModel_, "");
			XMVECTOR MagicRoot = Model::GetBonePosition(hModel_, "");
			//

			pFireMagic->Shot(shotPos, shotPos - MagicRoot);
		}
	}


	//Rキーが押されたら
	if (Input::IsKeyDown(DIK_R))
	{
		//MPが5以上あれば
		if (MP_PL > 5)
		{
			if (HP_PL < 100)
			{
				//回復する
				HP_PL = HP_PL + 10;
				MP_PL -= 5;
			}
		}
	}

	if (HP_PL > 100)
	{
		//HPが100以上回復することはない
		int a = HP_PL - 100;

		HP_PL = HP_PL - a;
	}

	Slime* pSlime_ = (Slime*)FindObject("Slime");
	Dog* pDog_ = (Dog*)FindObject("Dog");
	Goblin* pGoblin_ = (Goblin*)FindObject("Goblin");
	bird* pbird_ = (bird*)FindObject("bird");
	Minotaur* pMinotaur_ = (Minotaur*)FindObject("Minotaur");
	Dragon* pDragon_ = (Dragon*)FindObject("Dragon");
	FireAttack_DR* pFAR_ = (FireAttack_DR*)FindObject("Fireattack_DR");


	if (flag2 == 1)
	{
		//回避したら1秒間無敵
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time2 == TimerFlag_2)
			{
				switch (time2)
				{

				case 1:TimerFlag_2 = 1; break;
				case 0:TimerFlag_2 = 0;
				}
				time2--;
				TimerFlag_2--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_2 == 0)
		{
			pSlime_->AT_SL = 3;
			pDog_->AT_DG = 7;
			pGoblin_->AT_GB = 14;
			pbird_->AT_BI = 15;
			pMinotaur_->AT_MI = 20;
			pDragon_->AT_DR = 25;
			pFAR_->AT_FA = 20;
			flag2 = 0;
			time2 = 1;
			TimerFlag_2 = 1;
		}
	}

	if (flag7 == 5)
	{
		//4回攻撃したら1秒間攻撃できなくなる
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time7 == TimerFlag_7)
			{
				switch (time7)
				{
				case 1:TimerFlag_7 = 1; break;
				case 0:TimerFlag_7 = 0;
				}
				time7--;
				TimerFlag_7--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_7 == 0)
		{
			flag7 = 0;
			time7 = 1;
			TimerFlag_7 = 1;
		}
	}
	//
}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}

//何かに当たった
void Player::OnCollision(GameObject * pTarget)
{
	Slime* pSlime_ = (Slime*)FindObject("Slime");
	Dog* pDog_ = (Dog*)FindObject("Dog");
	Goblin* pGoblin_ = (Goblin*)FindObject("Goblin");
	bird* pbird_ = (bird*)FindObject("bird");
	Minotaur* pMinotaur_ = (Minotaur*)FindObject("Minotaur");
	Dragon* pDragon_ = (Dragon*)FindObject("Dragon");
	Ground* pGround_ = (Ground*)FindObject("Ground");

	XMVECTOR move_w = { 0.0f, 0.0f, 0.2f, 0.0f };
	XMVECTOR move_d = { 0.2f, 0.0f, 0.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_w = XMVector3TransformCoord(move_w, mat);
	move_d = XMVector3TransformCoord(move_d, mat);

	//スライムに当たったとき
	if (pTarget->GetObjectName() == "Slime")
	{



		//体力が0以下になると
		if (HP_PL < 1)
		{
			//ゲームオーバー
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER1);
		}


		if (Input::IsKey(DIK_W))
		{
			transform_.position_ -= move_w;
		}

		if (Input::IsKey(DIK_S))
		{
			transform_.position_ += move_w;
		}

		if (Input::IsKey(DIK_A))
		{
			transform_.position_ += move_d;
		}

		if (Input::IsKey(DIK_D))
		{
			transform_.position_ -= move_d;
		}


		//左シフトキーが押されていたら
		if (Input::IsKey(DIK_LSHIFT))
		{
			if (Input::IsKey(DIK_W))
			{
				transform_.position_ -= move_w * 2;
			}

			if (Input::IsKey(DIK_S))
			{
				transform_.position_ += move_w * 2;
			}

			if (Input::IsKey(DIK_A))
			{
				transform_.position_ += move_d * 2;
			}

			if (Input::IsKey(DIK_D))
			{
				transform_.position_ -= move_d * 2;
			}
		}
	}

	//犬に当たったとき
	if (pTarget->GetObjectName() == "Dog")
	{



		//体力が0以下になると
		if (HP_PL < 1)
		{
			//ゲームオーバー
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER2);
		}

		if (Input::IsKey(DIK_W))
		{
			transform_.position_ -= move_w;
		}

		if (Input::IsKey(DIK_S))
		{
			transform_.position_ += move_w;
		}

		if (Input::IsKey(DIK_A))
		{
			transform_.position_ += move_d;
		}

		if (Input::IsKey(DIK_D))
		{
			transform_.position_ -= move_d;
		}


		//左シフトキーが押されていたら
		if (Input::IsKey(DIK_LSHIFT))
		{
			if (Input::IsKey(DIK_W))
			{
				transform_.position_ -= move_w * 2;
			}

			if (Input::IsKey(DIK_S))
			{
				transform_.position_ += move_w * 2;
			}

			if (Input::IsKey(DIK_A))
			{
				transform_.position_ += move_d * 2;
			}

			if (Input::IsKey(DIK_D))
			{
				transform_.position_ -= move_d * 2;
			}
		}
	}

	//ゴブリンに当たったとき
	if (pTarget->GetObjectName() == "Goblin")
	{



		//体力が0以下になると
		if (HP_PL < 1)
		{
			//ゲームオーバー
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER3);
		}

		if (Input::IsKey(DIK_W))
		{
			transform_.position_ -= move_w;
		}

		if (Input::IsKey(DIK_S))
		{
			transform_.position_ += move_w;
		}

		if (Input::IsKey(DIK_A))
		{
			transform_.position_ += move_d;
		}

		if (Input::IsKey(DIK_D))
		{
			transform_.position_ -= move_d;
		}


		//左シフトキーが押されていたら
		if (Input::IsKey(DIK_LSHIFT))
		{
			if (Input::IsKey(DIK_W))
			{
				transform_.position_ -= move_w * 2;
			}

			if (Input::IsKey(DIK_S))
			{
				transform_.position_ += move_w * 2;
			}

			if (Input::IsKey(DIK_A))
			{
				transform_.position_ += move_d * 2;
			}

			if (Input::IsKey(DIK_D))
			{
				transform_.position_ -= move_d * 2;
			}
		}
	}

	//鳥に当たったとき
	if (pTarget->GetObjectName() == "bird")
	{



		//体力が0以下になると
		if (HP_PL < 1)
		{
			//ゲームオーバー
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER4);
		}

		if (Input::IsKey(DIK_W))
		{
			transform_.position_ -= move_w;
		}

		if (Input::IsKey(DIK_S))
		{
			transform_.position_ += move_w;
		}

		if (Input::IsKey(DIK_A))
		{
			transform_.position_ += move_d;
		}

		if (Input::IsKey(DIK_D))
		{
			transform_.position_ -= move_d;
		}


		//左シフトキーが押されていたら
		if (Input::IsKey(DIK_LSHIFT))
		{
			if (Input::IsKey(DIK_W))
			{
				transform_.position_ -= move_w * 2;
			}

			if (Input::IsKey(DIK_S))
			{
				transform_.position_ += move_w * 2;
			}

			if (Input::IsKey(DIK_A))
			{
				transform_.position_ += move_d * 2;
			}

			if (Input::IsKey(DIK_D))
			{
				transform_.position_ -= move_d * 2;
			}
		}
	}

	//ミノタウロスに当たったとき
	if (pTarget->GetObjectName() == "Minotaur")
	{



		//体力が0以下になると
		if (HP_PL < 1)
		{
			//ゲームオーバー
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER5);
		}

		if (Input::IsKey(DIK_W))
		{
			transform_.position_ -= move_w;
		}

		if (Input::IsKey(DIK_S))
		{
			transform_.position_ += move_w;
		}

		if (Input::IsKey(DIK_A))
		{
			transform_.position_ += move_d;
		}

		if (Input::IsKey(DIK_D))
		{
			transform_.position_ -= move_d;
		}


		//左シフトキーが押されていたら
		if (Input::IsKey(DIK_LSHIFT))
		{
			if (Input::IsKey(DIK_W))
			{
				transform_.position_ -= move_w * 2;
			}

			if (Input::IsKey(DIK_S))
			{
				transform_.position_ += move_w * 2;
			}

			if (Input::IsKey(DIK_A))
			{
				transform_.position_ += move_d * 2;
			}

			if (Input::IsKey(DIK_D))
			{
				transform_.position_ -= move_d * 2;
			}
		}
	}

	//ドラゴンに当たったとき
	if (pTarget->GetObjectName() == "Dragon")
	{



		//体力が0以下になると
		if (HP_PL < 1)
		{
			//ゲームオーバー
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER6);
		}

		if (Input::IsKey(DIK_W))
		{
			transform_.position_ -= move_w;
		}

		if (Input::IsKey(DIK_S))
		{
			transform_.position_ += move_w;
		}

		if (Input::IsKey(DIK_A))
		{
			transform_.position_ += move_d;
		}

		if (Input::IsKey(DIK_D))
		{
			transform_.position_ -= move_d;
		}


		//左シフトキーが押されていたら
		if (Input::IsKey(DIK_LSHIFT))
		{
			if (Input::IsKey(DIK_W))
			{
				transform_.position_ -= move_w * 2;
			}

			if (Input::IsKey(DIK_S))
			{
				transform_.position_ += move_w * 2;
			}

			if (Input::IsKey(DIK_A))
			{
				transform_.position_ += move_d * 2;
			}

			if (Input::IsKey(DIK_D))
			{
				transform_.position_ -= move_d * 2;
			}
		}
	}

	//あれに当たったとき
	if (pTarget->GetObjectName() == "Ground")
	{
		if (Input::IsKey(DIK_W))
		{
			transform_.position_ -= move_w;
		}

		if (Input::IsKey(DIK_S))
		{
			transform_.position_ += move_w;
		}

		if (Input::IsKey(DIK_A))
		{
			transform_.position_ += move_d;
		}

		if (Input::IsKey(DIK_D))
		{
			transform_.position_ -= move_d;
		}
	}
	//
}

void Player::CountDown()
{
	time1 -= 1;

	time2 -= 1;

	time3 -= 1;

	time4 -= 1;

	time5 -= 1;

	time6 -= 1;

	time7 -= 1;
}
