#pragma once
#include "Engine/GameObject.h"

//MPバーを管理するクラス
class MPBar : public GameObject
{
	int hPict_[51];    //画像番号

	int MPflag = 50;

public:



	//コンストラクタ
	MPBar(GameObject* parent);

	//デストラクタ
	~MPBar();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


};