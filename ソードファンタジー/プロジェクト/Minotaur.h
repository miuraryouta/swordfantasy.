#pragma once
#include "Engine/GameObject.h"

//ミノタウロス(中ボス)を管理するクラス
class Minotaur : public GameObject
{
	int hModel_;    //モデル番号
	const int VISION_MI = 1;  //視野

	int time;

	int time2;

	int time3;

	char TimerFlag_MI = 5;

	char TimerFlag_MI2 = 1;

	char TimerFlag_MI3 = 10;

	//移動処理
	//引数：なし
	//戻り値：なし
	void Move();

	//移動処理以外
	//引数：なし
	//戻り値：なし
	void Special();

public:
	//コンストラクタ
	Minotaur(GameObject* parent);

	//デストラクタ
	~Minotaur();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ステータス
	int HP_MI = 200; //体力

	int AT_MI = 20; //攻撃力

	//乱数
	int Rand_MI;

	//フラグ
	int flag = 0;

	int flag2 = 0;

	int flag3 = 0;

	int flag4 = 0;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void CountDown();
};