#include "Slime.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Sword.h"
#include "Engine/SceneManager.h"
#include "Player.h"
#include "FireMagic.h"
#include "Engine/Input.h"
#include "Player.h"
#include "Ground.h"

#include <time.h>






//コンストラクタ
Slime::Slime(GameObject * parent)
	:GameObject(parent, "Slime"), hModel_(-1)
{
}

//デストラクタ
Slime::~Slime()
{
}

//初期化
void Slime::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Slime.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = 0;
	transform_.position_.vecY = 0;
	transform_.position_.vecZ = 25;

	transform_.scale_.vecX = 4.0f;
	transform_.scale_.vecY = 4.0f;
	transform_.scale_.vecZ = 4.0f;

	//球型コライダー
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0.8, 0, 0), 0.8f);
	AddCollider(collision);

	time = 5;

	time2 = 1;

	time3 = 10;

	time4 = 1;

	Model::SetAnimFrame(hModel_, 1, 150, 2);
}

//更新
void Slime::Update()
{
	Move();     //移動処理

	Special();     //移動処理以外
}

void Slime::Move()
{
	Player* pPlayer;
	pPlayer = (Player*)FindObject("Player");
	XMVECTOR direction = { 0.0f, 0.0f, -1.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	direction = XMVector3TransformCoord(direction, mat);

	XMVECTOR distance = pPlayer->GetPosition() - transform_.position_;



	distance = XMVector3Normalize(distance);
	direction = XMVector3Normalize(direction);

	float naiseki = XMVector3Dot(direction, distance).vecX;
	float angle = acos(naiseki);


	angle = XMConvertToDegrees(angle);



	XMVECTOR move_sl = { 0.0f, 0.0f, 0.1f, 0.0f };
	

	XMMATRIX mat_sl;
	mat_sl = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_sl = XMVector3TransformCoord(move_sl, mat_sl);




	if (VISION_S >= angle)
	{
		//プレイヤーのいる方向に向かってゆく
		transform_.position_ -= move_sl;
		
		Model::SetAnimFrame(hModel_, 1, 150, 2);
	}
	else if (VISION_S <= angle)
	{
		transform_.rotate_.vecY += 2.0f;
	}
	else if (VISION_S > angle)
	{
		transform_.rotate_.vecY += 2.0f;
	}

	//




	//飛んで着地した後1秒待つ
	if (flag_SL4 == 1)
	{
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_SL4)
			{
				switch (time)
				{
				case 1:TimerFlag_SL4 = 1; break;
				case 0:TimerFlag_SL4 = 0;
				}
				time4--;
				TimerFlag_SL4--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_SL4 == 0)
		{
			flag_SL4 = 0;
			time4 = 1;
			TimerFlag_SL4 = 1;
		}
	}
}

void Slime::Special()
{

	//HPが0以下になったとき
	if (HP_SL < 1)
	{
		KillMe(); //スライムが死ぬ

		//クリアシーンに行く
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR1);

	}




	XMVECTOR move_sl = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_sl;
	mat_sl = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_sl = XMVector3TransformCoord(move_sl, mat_sl);

	if (flag_SL == 1)
	{
		AT_SL = 0;

		transform_.position_ += move_sl;

		//攻撃した後5秒間攻撃しない
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_SL)
			{


				switch (time)
				{
				case 5:TimerFlag_SL = 5; break;
				case 4:TimerFlag_SL = 4; break;
				case 3:TimerFlag_SL = 3; break;
				case 2:TimerFlag_SL = 2; break;
				case 1:TimerFlag_SL = 1; break;
				case 0:TimerFlag_SL = 0;
				}
				time--;
				TimerFlag_SL--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_SL == 0)
		{

			AT_SL = 3;
			flag_SL = 0;
			time = 5;
			TimerFlag_SL = 5;
		}
	}

	//特殊攻撃
	if (flag_SL3 == 1)
	{
		if (Rand_SL == 1)
		{
			AT_SL += 5;

			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_SL3)
				{
					switch (time3)
					{
					case 10:TimerFlag_SL3 = 10; break;
					case 9:TimerFlag_SL3 = 9; break;
					case 8:TimerFlag_SL3 = 8; break;
					case 7:TimerFlag_SL3 = 7; break;
					case 6:TimerFlag_SL3 = 6; break;
					case 5:TimerFlag_SL3 = 5; break;
					case 4:TimerFlag_SL3 = 4; break;
					case 3:TimerFlag_SL3 = 3; break;
					case 2:TimerFlag_SL3 = 2; break;
					case 1:TimerFlag_SL3 = 1; break;
					case 0:TimerFlag_SL3 = 0;
					}
					time3--;
					TimerFlag_SL3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_SL3 == 0)
			{
				Rand_SL = 0;
				AT_SL -= 5;
				flag_SL3 = 0;
				time3 = 10;
				TimerFlag_SL3 = 10;
			}
		}
		else if (Rand_SL > 1)
		{
			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_SL3)
				{
					switch (time3)
					{
					case 10:TimerFlag_SL3 = 10; break;
					case 9:TimerFlag_SL3 = 9; break;
					case 8:TimerFlag_SL3 = 8; break;
					case 7:TimerFlag_SL3 = 7; break;
					case 6:TimerFlag_SL3 = 6; break;
					case 5:TimerFlag_SL3 = 5; break;
					case 4:TimerFlag_SL3 = 4; break;
					case 3:TimerFlag_SL3 = 3; break;
					case 2:TimerFlag_SL3 = 2; break;
					case 1:TimerFlag_SL3 = 1; break;
					case 0:TimerFlag_SL3 = 0;
					}
					time3--;
					TimerFlag_SL3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_SL3 == 0)
			{
				Rand_SL = 0;
				flag_SL3 = 0;
				time3 = 10;
				TimerFlag_SL3 = 10;
			}
		}
	}
}


//描画
void Slime::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Slime::Release()
{
}

void Slime::OnCollision(GameObject * pTarget)
{
	Sword* pSword_ = (Sword*)FindObject("Sword");
	FireMagic* pFireMagic_ = (FireMagic*)FindObject("FireMagic");
	Player* pPlayer_ = (Player*)FindObject("Player");
	Ground* pGround_ = (Ground*)FindObject("Ground");

	XMVECTOR move_sl = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_sl;
	mat_sl = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_sl = XMVector3TransformCoord(move_sl, mat_sl);



	//剣に当たったとき
	if (pTarget->GetObjectName() == "Sword")
	{
		//攻撃はマウスの左クリックのほうがいいかも
		//Eキーを押した
		if (Input::IsKeyDown(DIK_E))
		{
			if (pPlayer_->flag7 != 5)
			{


				pPlayer_->MP_PL += 5;

				if (pPlayer_->flag1 == 0)
				{
					pPlayer_->SP_PL += 5;
				}

				//HPが減る
				HP_SL = HP_SL - pSword_->AT_S;

				pPlayer_->flag7 += 1;
				
			}
		}
		


	}

	//魔法にあたったとき
	if (pTarget->GetObjectName() == "FireMagic")
	{
		HP_SL = HP_SL - pFireMagic_->AT_FM;
		pTarget->KillMe(); //魔法が消える
	}



	//プレイヤーに当たったら
	if (pTarget->GetObjectName() == "Player")
	{




		transform_.position_ += move_sl;

		if (flag_SL2 == 0)
		{
			flag_SL2 = 1;
		}
		else if (flag_SL2 == 1)
		{
			AT_SL = 0;

			//攻撃前は1秒間攻撃しない
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time2 == TimerFlag_SL2)
				{


					switch (time2)
					{
					case 1:TimerFlag_SL2 = 1; break;
					case 0:TimerFlag_SL2 = 0;
					}
					time2--;
					TimerFlag_SL2--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_SL2 == 0)
			{

				AT_SL = 3;
				flag_SL2 = 0;
				time2 = 1;
				TimerFlag_SL2 = 1;
			}
		}


		

		if (flag_SL == 0)
		{
			
			pPlayer_->HP_PL = pPlayer_->HP_PL - AT_SL;
			flag_SL = 1;
		}


		if (flag_SL3 == 0)
		{
			Rand_SL = rand() % 10;
			flag_SL3 = 1;
		}


		

		


			
			

	}

	//に当たったとき
	if (pTarget->GetObjectName() == "Ground")
	{
		transform_.position_ += move_sl;
	}







}

void Slime::CountDown()
{
	time -= 1;

	time2 -= 1;

	time3 -= 1;

	time4 -= 1;
}






