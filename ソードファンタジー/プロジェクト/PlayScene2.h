#pragma once
#include "Engine/GameObject.h"


//プレイシーンシーン2を管理するクラス
class PlayScene2 : public GameObject
{
	int hPict_;    //画像番号
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene2(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};