#include "Background.h"
#include "Engine/Image.h"
#include "Engine/Model.h"



//コンストラクタ
Background::Background(GameObject* parent)
	:GameObject(parent, "Background"), hModel_(-1)
{
}

//デストラクタ
Background::~Background()
{
}

//初期化
void Background::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Background2.fbx");
	assert(hModel_ >= 0);

	//transform_.position_.vecY -= 0.5;


}

//更新
void Background::Update()
{


}




//描画
void Background::Draw()
{
	
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Background::Release()
{
}












