#pragma once
#include "Engine/GameObject.h"

//スライムを管理するクラス
class Slime : public GameObject
{
	int hModel_;    //モデル番号
	
	const int VISION_S = 1;  //視野

	const int VISION_SS = 60;  //回転用の視野的なもの

	const float GRAVITY_SL = 0.03f;  //重力

	int time;

	int time2;

	int time3;

	int time4;

	//移動処理
    //引数：なし
    //戻り値：なし
	void Move();

	//移動処理以外
	//引数：なし
	//戻り値：なし
	void Special();



public:



	//コンストラクタ
	Slime(GameObject* parent);

	//デストラクタ
	~Slime();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void CountDown();

	

	
	//乱数
	int Rand_SL;

	int Rand_SLA; //攻撃用

	//ステータス
	int HP_SL = 50; //体力

	int AT_SL = 3; //攻撃力

	//フラグ
	int flag_SL = 0;

	char TimerFlag_SL = 5;

	int flag_SL2 = 0;

	char TimerFlag_SL2 = 1;

	int flag_SL3 = 0;

	char TimerFlag_SL3 = 10;

	int flag_SL4 = 0;

	char TimerFlag_SL4 = 1;

	int flag_jp = 0;   //ジャンプ用のフラグ
};