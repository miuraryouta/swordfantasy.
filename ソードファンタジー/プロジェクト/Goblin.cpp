#include "Goblin.h"
#include "Sword.h"
#include "Player.h"
#include "FireMagic.h"
#include "Engine/Model.h"
#include "Engine/SceneManager.h"
#include "Engine/BoxCollider.h"
#include "Engine/Input.h"
#include "Ground.h"






//コンストラクタ
Goblin::Goblin(GameObject* parent)
	:GameObject(parent, "Goblin"), hModel_(-1)
{
}

//デストラクタ
Goblin::~Goblin()
{
}

//初期化
void Goblin::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Goblin.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = 0;
	transform_.position_.vecY = 0;
	transform_.position_.vecZ = 25;

	transform_.rotate_.vecY = 180;

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 1.5, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	time = 3;

	time2 = 1;

	time3 = 10;

	Model::SetAnimFrame(hModel_, 1, 150, 2);
}

//更新
void Goblin::Update()
{
	Move();   //移動処理

	Special();    //移動処理以外

	

	
}

void Goblin::Move()
{
	Player* pPlayer;
	pPlayer = (Player*)FindObject("Player");
	XMVECTOR direction = { 0.0f, 0.0f, 1.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	direction = XMVector3TransformCoord(direction, mat);

	XMVECTOR distance = pPlayer->GetPosition() - transform_.position_;



	distance = XMVector3Normalize(distance);
	direction = XMVector3Normalize(direction);

	float naiseki = XMVector3Dot(direction, distance).vecX;
	float angle = acos(naiseki);


	angle = XMConvertToDegrees(angle);



	XMVECTOR move_gb = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_gb;
	mat_gb = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_gb = XMVector3TransformCoord(move_gb, mat_gb);



	if (VISION_GB >= angle)
	{
		//プレイヤーのいる方向に向かってゆく
		transform_.rotate_.vecY += 0;

		transform_.position_ += move_gb;
	}
	else if(VISION_GB > angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY += 2.0f;
	}
	else if (VISION_GB < angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY -= 2.0f;
	}

}

void Goblin::Special()
{
	//HPが0以下になったとき
	if (HP_GB < 1)
	{
		KillMe(); //ゴブリンが死ぬ

		//シーンが変わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR3);

	}


	XMVECTOR move_gb = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_gb;
	mat_gb = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_gb = XMVector3TransformCoord(move_gb, mat_gb);

	if (flag == 1)
	{
		AT_GB = 0;

		transform_.position_ -= move_gb;

		//攻撃した後3秒間攻撃しない
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_GB)
			{


				switch (time)
				{
				case 3:TimerFlag_GB = 3; break;
				case 2:TimerFlag_GB = 2; break;
				case 1:TimerFlag_GB = 1; break;
				case 0:TimerFlag_GB = 0;
				}
				time--;
				TimerFlag_GB--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_GB == 0)
		{
			AT_GB = 14;
			flag = 0;
			time = 3;
			TimerFlag_GB = 3;
		}
	}


	//特殊攻撃
	if (flag3 == 0)
	{
		Rand_GB = rand() % 10;
		flag3 = 1;
	}
	else if (flag3 == 1)
	{
		if (Rand_GB == 1)
		{
			AT_GB += 5;

			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_GB3)
				{
					switch (time3)
					{
					case 10:TimerFlag_GB3 = 10; break;
					case 9:TimerFlag_GB3 = 9; break;
					case 8:TimerFlag_GB3 = 8; break;
					case 7:TimerFlag_GB3 = 7; break;
					case 6:TimerFlag_GB3 = 6; break;
					case 5:TimerFlag_GB3 = 5; break;
					case 4:TimerFlag_GB3 = 4; break;
					case 3:TimerFlag_GB3 = 3; break;
					case 2:TimerFlag_GB3 = 2; break;
					case 1:TimerFlag_GB3 = 1; break;
					case 0:TimerFlag_GB3 = 0;
					}
					time3--;
					TimerFlag_GB3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_GB3 == 0)
			{
				Rand_GB = 0;
				AT_GB -= 5;
				flag3 = 0;
				time3 = 10;
				TimerFlag_GB3 = 10;
			}
		}
		else if (Rand_GB > 1)
		{
			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_GB3)
				{
					switch (time3)
					{
					case 10:TimerFlag_GB3 = 10; break;
					case 9:TimerFlag_GB3 = 9; break;
					case 8:TimerFlag_GB3 = 8; break;
					case 7:TimerFlag_GB3 = 7; break;
					case 6:TimerFlag_GB3 = 6; break;
					case 5:TimerFlag_GB3 = 5; break;
					case 4:TimerFlag_GB3 = 4; break;
					case 3:TimerFlag_GB3 = 3; break;
					case 2:TimerFlag_GB3 = 2; break;
					case 1:TimerFlag_GB3 = 1; break;
					case 0:TimerFlag_GB3 = 0;
					}
					time3--;
					TimerFlag_GB3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_GB3 == 0)
			{
				Rand_GB = 0;
				flag3 = 0;
				time3 = 10;
				TimerFlag_GB3 = 10;
			}
		}
	}
}

//描画
void Goblin::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Goblin::Release()
{
}

//何かに当たった
void Goblin::OnCollision(GameObject * pTarget)
{
	
	Sword* pSword_ = (Sword*)FindObject("Sword");
	FireMagic* pFireMagic_ = (FireMagic*)FindObject("FireMagic");
	Player* pPlayer_ = (Player*)FindObject("Player");
	Ground* pGround_ = (Ground*)FindObject("Ground");

	XMVECTOR move_gb = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_gb;
	mat_gb = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_gb = XMVector3TransformCoord(move_gb, mat_gb);


	//剣に当たったとき
	if (pTarget->GetObjectName() == "Sword")
	{
		//Eキーを押した
		if (Input::IsKeyDown(DIK_E))
		{
			if (pPlayer_->flag7 != 5)
			{


				pPlayer_->MP_PL += 5;

				if (pPlayer_->flag1 == 0)
				{
					pPlayer_->SP_PL += 5;
				}

				//HPが減る
				HP_GB = HP_GB - pSword_->AT_S;

				pPlayer_->flag7 += 1;
			}
		}



	}

	//魔法に当たったとき
	if (pTarget->GetObjectName() == "FireMagic")
	{
		HP_GB = HP_GB - pFireMagic_->AT_FM;
		pTarget->KillMe(); //魔法が消える
	}

	//プレイヤーにあったとき
	if (pTarget->GetObjectName() == "Player")
	{


		transform_.position_ -= move_gb;

		if (flag2 == 0)
		{
			flag2 = 1;
		}
		else if (flag2 == 1)
		{
			AT_GB = 0;

			

			//攻撃前1秒間攻撃しない
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time2 == TimerFlag_GB2)
				{


					switch (time2)
					{
					case 1:TimerFlag_GB2 = 1; break;
					case 0:TimerFlag_GB2 = 0;
					}
					time2--;
					TimerFlag_GB2--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_GB2 == 0)
			{
				AT_GB = 14;
				flag2 = 0;
				time2 = 1;
				TimerFlag_GB2 = 1;
			}
		}

		if (flag == 0)
		{
			//フラグが0のときプレイヤーに攻撃できる
			pPlayer_->HP_PL = pPlayer_->HP_PL - AT_GB;
			flag = 1;
		}



	}

	//に当たったとき
	if (pTarget->GetObjectName() == "Ground")
	{
		transform_.position_ -= move_gb;
	}
}

void Goblin::CountDown()
{
	time -= 1;

	time2 -= 1;

	time3 -= 1;
}
