#pragma once
#include "Engine/GameObject.h"

//ドラゴン(ボス)を管理するクラス
class Dragon : public GameObject
{
	int hModel_;    //モデル番号
	
	const int VISION_DR = 1;  //視野

	int time;

	int time2;

	char TimerFlag_DR = 5;

	char TimerFlag_DR2 = 1;

	//移動処理
	//引数：なし
	//戻り値：なし
	void Move();

	//移動処理以外
	//引数：なし
	//戻り値：なし
	void Special();

public:
	//コンストラクタ
	Dragon(GameObject* parent);

	//デストラクタ
	~Dragon();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ステータス
	int HP_DR = 400; //体力

	int AT_DR = 25; //攻撃力

	//フラグ
	int flag = 0;

	int flag2 = 0;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void CountDown();
};