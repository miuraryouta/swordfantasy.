#pragma once
#include "Engine/GameObject.h"

//剣を管理するクラス
class Sword : public GameObject
{
	int hModel_;    //モデル番号
	int hSound_;    //サウンド番号

public:



	//コンストラクタ
	Sword(GameObject* parent);

	//デストラクタ
	~Sword();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;



	//開放
	void Release() override;

	//ステータス
	int AT_S = 10; //攻撃力

};