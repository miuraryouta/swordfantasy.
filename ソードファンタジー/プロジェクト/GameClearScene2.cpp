#include "GameClearScene2.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "Engine/Audio.h"

//コンストラクタ
GameClearScene2::GameClearScene2(GameObject* parent)
	: GameObject(parent, "GameClearScene2"), hPict_(-1)
{
}

//初期化
void GameClearScene2::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("GameClear2.png");
	assert(hPict_ >= 0);

	transform_.scale_.vecX = 3.0f;
	transform_.scale_.vecY = 3.0f;
}

//更新
void GameClearScene2::Update()
{
	//エンターキーが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//タイトルシーンに行く
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}

	//スペースキーが押されていたら
	if (Input::IsKey(DIK_SPACE))
	{
		//プレイシーン2に行く
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY3);
	}
}

//描画
void GameClearScene2::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameClearScene2::Release()
{
}