#pragma once
#include "Engine/GameObject.h"

//魔法を管理するクラス
class FireMagic : public GameObject
{
	int hModel_;    //モデル番号
	XMVECTOR move_;
	const float SPEED = 0.5f;     //速さ
public:



	//コンストラクタ
	FireMagic(GameObject* parent);

	//デストラクタ
	~FireMagic();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Shot(XMVECTOR position, XMVECTOR direction);

	//ステータス
	int AT_FM = 10; //攻撃力
};